﻿namespace TransportManagementSystem
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using TransportManagementSystem.Services.Clients;
    using TransportManagementSystem.Services.Messages;
    using TransportManagementSystem.Services.Services;

    public class Program
    {
        public static async Task Main(string[] args)
        {
            int capacity = 0;
            var service = InitService();

            while(true)
            {
                Console.Write("Input capacity: ");
                var isValidInt = int.TryParse(Console.ReadLine(), out capacity);

                if (capacity < 1 || !isValidInt)
                {
                    Console.WriteLine("Invalid input! Please try again.");
                } 
                else
                {
                    Console.WriteLine("Requesting...");
                    var response = await service.GetAvailableStarships(capacity);
                    Display(response);
                }

                Console.WriteLine("===============================================\n");
            }
        }

        private static IStarshipSevice InitService()
        {
            var configs = new Dictionary<string, string>()
            {
                {"baseUrl", "https://swapi.dev/api/" }
            };

            var httpClient = new HttpClient();
            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configs)
                .Build();

            var client = new SWApiClient(httpClient, configuration);
            var service = new StarshipService(client);

            return service;
        }

        private static void Display(GetAvailableStarshipsResponse response)
        {
            if (!response.IsSuccessful)
            {
                Console.WriteLine();
                return;
            }

            Console.WriteLine("\nResult:");
            foreach (var starship in response.AvailableStarships)
            {
                Console.WriteLine($"{starship.Name} - {starship.Pilot}");
            }

            if (response.AvailableStarships.Count == 0)
            {
                Console.WriteLine("There are no available starships with the given capacity.");
            }

            Console.WriteLine("");
        }
    }
}
