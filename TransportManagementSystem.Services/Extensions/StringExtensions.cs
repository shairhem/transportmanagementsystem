﻿namespace TransportManagementSystem.Services.Extensions
{
    public static class StringExtensions
    {
        public static int ParseInt(this string prop)
        {
            int result = 0;
            prop = prop
                .Replace(",", "")
                .Trim();

            int.TryParse(prop, out result);

            return result;
        }
    }
}
