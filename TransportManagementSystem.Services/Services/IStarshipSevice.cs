﻿namespace TransportManagementSystem.Services.Services
{
    using System.Threading.Tasks;
    using TransportManagementSystem.Services.Messages;

    public interface IStarshipSevice
    {
        Task<GetAvailableStarshipsResponse> GetAvailableStarships(int capacity);
    }
}
