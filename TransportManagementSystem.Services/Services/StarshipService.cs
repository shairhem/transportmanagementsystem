﻿namespace TransportManagementSystem.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using TransportManagementSystem.Services.Clients;
    using TransportManagementSystem.Services.Extensions;
    using TransportManagementSystem.Services.Messages;
    using TransportManagementSystem.Services.Models;

    public class StarshipService : IStarshipSevice
    {
        private readonly ISWApiClient swApiClient;

        public StarshipService(ISWApiClient swApiClient)
        {
            this.swApiClient = swApiClient;
        }

        public async Task<GetAvailableStarshipsResponse> GetAvailableStarships(int capacity)
        {

            var result = new GetAvailableStarshipsResponse();
            try
            {
                var availableStarships = new List<Starship>();
                var starships = await this.swApiClient.GetStarships();
                var filteredStarships = starships
                    .Select(s => new
                    {
                        s.Name,
                        capacity = s.PassengerCapacity.ParseInt(),
                        s.PilotLinks
                    })
                    .Where(s => s.capacity >= capacity && s.PilotLinks.Count > 0)
                    .ToList();

                foreach (var starship in filteredStarships)
                {
                    var pilots = await this.swApiClient.GetPilots(starship.PilotLinks);
                    availableStarships.AddRange(pilots.Select(p => new Starship() { Name = starship.Name, Pilot = p.Name }));
                }

                result.AvailableStarships = availableStarships;
                result.IsSuccessful = true;
            }
            catch (Exception)
            {
                result.IsSuccessful = false;
                result.Message = "Retrieval of available starships failed. Please try again. Please contact support if problem persists.";
            }
            
            return result;
        }
    }
}
