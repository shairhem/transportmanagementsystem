﻿namespace TransportManagementSystem.Services.Clients
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using TransportManagementSystem.Services.Models;

    public class SWApiClient : ISWApiClient
    {
        private readonly HttpClient httpClient;
        private readonly IConfiguration configuration;

        public SWApiClient(HttpClient httpClient, IConfiguration configuration)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        public async Task<ICollection<PilotApiResponse>> GetPilots(ICollection<string> pilotLinks)
        {
            var pilots = new List<PilotApiResponse>();
            foreach (var link in pilotLinks)
            {
                var pilot = await this.SendGetRequest<PilotApiResponse>(link);
                pilots.Add(pilot);
            }

            return pilots;
        }

        public async Task<ICollection<StarshipApiResponse>> GetStarships()
        {
            var starships = new List<StarshipApiResponse>();
            var requestUrl = $"{this.configuration.GetValue<string>("baseUrl")}starships/";
            while(!string.IsNullOrWhiteSpace(requestUrl))
            {
                var response = await this.SendGetRequest<SWApiListResponse<StarshipApiResponse>>(requestUrl);
                requestUrl = response.Next;
                starships.AddRange(response.Results);
            }

            return starships;
        }

        private async Task<T> SendGetRequest<T>(string requestUrl)
        {
            var apiResponse = await this.httpClient.GetAsync(requestUrl);
            var payload = await apiResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(payload);
            return result;
        }
    }
}
