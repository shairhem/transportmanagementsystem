﻿namespace TransportManagementSystem.Services.Clients
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using TransportManagementSystem.Services.Models;

    public interface ISWApiClient
    {
        Task<ICollection<StarshipApiResponse>> GetStarships();

        Task<ICollection<PilotApiResponse>> GetPilots(ICollection<string> pilotLinks);
    }
}
