﻿namespace TransportManagementSystem.Services.Models
{
    using Newtonsoft.Json;

    public class PilotApiResponse
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
