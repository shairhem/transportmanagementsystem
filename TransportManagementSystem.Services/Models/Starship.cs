﻿namespace TransportManagementSystem.Services.Models
{
    public class Starship
    {
        public string Name { get; set; }

        public string Pilot { get; set; }
    }
}
