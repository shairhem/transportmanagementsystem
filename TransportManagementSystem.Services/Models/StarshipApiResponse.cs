﻿namespace TransportManagementSystem.Services.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class StarshipApiResponse
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("passengers")]
        public string PassengerCapacity { get; set; }

        [JsonProperty("pilots")]
        public ICollection<string> PilotLinks { get; set; }
    }
}
