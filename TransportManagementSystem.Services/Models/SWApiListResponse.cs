﻿namespace TransportManagementSystem.Services.Models
{
    using System.Collections.Generic;

    public class SWApiListResponse<T>
    {
        public int Count { get; set; }

        public string Next { get; set; }

        public ICollection<T> Results { get; set; }
    }
}
