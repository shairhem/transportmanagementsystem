﻿namespace TransportManagementSystem.Services.Messages
{
    using System.Collections.Generic;
    using TransportManagementSystem.Services.Models;

    public class GetAvailableStarshipsResponse
    {
        public ICollection<Starship> AvailableStarships { get; set; }

        public bool IsSuccessful { get; set; }

        public string Message { get; set; }
    }
}
